const ex = require('express');
const path = require('path');
const http = require('http');
const WebSocket = require('ws');
const url = require('url');
const bp = require('body-parser');
const app = ex();

const port = process.env.PORT || 1337;
const server = http.createServer(app);
const wss = new WebSocket.Server({ server, path: '/connect' });


function heartbeat() {
  this.isAlive = true;
}

wss.on('connection', function connection(ws) {
  ws.isAlive = true;
  ws.on('pong', heartbeat);
});

// check to make sure socket is still alive and terminates ones that aren't
setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();

    ws.isAlive = false;
    ws.ping('', false, true);
  });
}, 30000);


wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  });
};

function sendAchievement(message) {
  wss.clients.forEach(function (client) {
    if (client.readyState === WebSocket.OPEN && client.id === message.id) {
      client.send(JSON.stringify(message));
    }
  });
}

function sendPresence(id, ws) {

  const others = [];
  let c;
  wss.clients.forEach(function (client) {
    if (client.readyState === WebSocket.OPEN) {
      if (client.id !== id) {
        others.push(client.id);
      } else {
        c = client;
      }
    }
  });

  const msg = {
    message: others,
    type: 'presence'
  }

    ws.send(JSON.stringify(msg));
  
}

wss.on('connection', function connection(ws, req) {
  const location = url.parse(req.url, true);
  ws.id = location.query.id;
  // You might use location.query.access_token to authenticate or share sessions
  // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)

  ws.on('message', function incoming(message) {
    const m = JSON.parse(message);
    if (m.type === 'chat') {
      wss.broadcast(message);
    } else if (m.type === 'achievement') {
      sendAchievement(m);
    } else if (m.type === 'presence') {
      sendPresence(m.id, this);
    }
  });

  const conn = {
    type: 'chat',
    message: 'has connected to chat!',
    id: ws.id
  };

  wss.broadcast(JSON.stringify(conn));
});


app.use(bp.json());
//setup static file responses
app.use(ex.static(path.join(__dirname, 'dist')));

app.get('/.well-known/acme-challenge/:fileid', function (req, res) {
  res.sendFile(__dirname + '/.well-known/acme-challenge/' + req.params.fileid);
})

app.post('/achievement', function (req, res) {
  var contype = req.headers['content-type'];
  if (!contype || contype.indexOf('application/json') !== 0 || !req.body) {
    res.statusCode = 400;
    return res.send('400: must be json');
  } else {
    if (req.body.type !== 'achievement' || !req.body.message || !req.body.id) {
      res.statusCode = 400;
      return res.send(' must have type of achievement, and a message, and an id: ' + JSON.stringify(req.body));
    } else {
      req.body.title = 'Achievement Unlocked!';
      // should actually find who the achievement belongs to and send it
      sendAchievement(req.body);
      res.statusCode = 200;
      res.send('sent achievement: ' + JSON.stringify(req.body));
    }
  }
});

app.use('/dist', ex.static(__dirname + '/dist'));

// not using routes don't need redirects
// app.all('/*', function (request, response, next) {
//   response.sendFile(__dirname + '/dist/index.html');
// });

server.listen(port);
console.log('Server Started on Port: ' + port);