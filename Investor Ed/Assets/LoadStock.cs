﻿using UnityEngine;
using System.Collections;

public class LoadStock : MonoBehaviour {
	public GameObject nameText;
	public GameObject numText;
	public Stock stock;

	public Stock get(){
		return stock;
	}

	public void Load(Stock b){
		stock = b;
		TextMesh name = nameText.GetComponent<TextMesh> ();
		TextMesh num = numText.GetComponent<TextMesh> ();
		name.text = b.name;
		num.text = "#"+ Random.Range (100000, 10000000);

	}
}
