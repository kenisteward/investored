﻿using UnityEngine;
using System.Collections;

public class LoadBrokerSheet : MonoBehaviour {
	Broker broker;

	public GameObject nameText;
	public GameObject ageText;
	public GameObject billsText;

	public Broker get(){
		return broker;
	}

	public void Load(Broker b){
		broker = b;
		TextMesh name = nameText.GetComponent<TextMesh> ();
		TextMesh age = ageText.GetComponent<TextMesh> ();
		TextMesh bills = billsText.GetComponent<TextMesh> ();
		name.text = b.name;
		age.text = b.age + " ";
		string billCollectText = "";
		foreach (string ket in b.bills.Keys) {
			billCollectText += ket + "\t" + b.bills [ket]+"\n";
		}
		bills.text = billCollectText;
	}
}
