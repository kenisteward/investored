﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayController : MonoBehaviour {

	public int cameraSelect = 1;
		
	// Update is called once per frame
	void Update () {
		//If the c button is pressed, switch to the next camera
		//Set the camera at the current index to inactive, and set the next one in the array to active
		//When we reach the end of the camera array, move back to the beginning or the array.

		if (Input.GetButtonDown("Fire1") || Input.touches.Length > 0){
			CastRay();
		}
	}

	void CastRay() {
		Vector3 mousePosition = Input.mousePosition;
		if (Input.touches.Length > 0 )
			mousePosition = Input.touches[0].position;
		Ray ray = CameraController.getMainCam ().ScreenPointToRay (mousePosition);
		RaycastHit2D hit01 = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		if (hit01.collider != null && hit01.transform == transform) {
			CameraController.changeCam (cameraSelect);
			Debug.Log ("Camera with name: " + CameraController.getCameras()[cameraSelect].ToString() + ", is now enabled");
		}
	}
}
