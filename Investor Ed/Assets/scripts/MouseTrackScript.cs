﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTrackScript : MonoBehaviour {

	private Vector3 mousePosition;
	public float moveSpeed = 0.1f;
	private bool track = false;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		/*if (track) {
			mousePosition = Input.mousePosition;
			mousePosition = Camera.main.ScreenToWorldPoint (mousePosition);
			Vector2 t = Vector2.Lerp (transform.position, mousePosition, moveSpeed);
			transform.position = new Vector3 (t.x, t.y, transform.position.z);// Vector2.Lerp (transform.position, mousePosition, moveSpeed);
		}
		if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Moved)
        {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.touches[0]
            */

		if (Input.touches.Length > 0 && track) {
			mousePosition = Input.touches[0].position;
			mousePosition = Camera.main.ScreenToWorldPoint (mousePosition);
			Vector2 t = Vector2.Lerp (transform.position, mousePosition, moveSpeed);
			transform.position = new Vector3 (t.x, t.y, transform.position.z);// Vector2.Lerp (transform.position, mousePosition, moveSpeed);
		}

		if (Input.GetButton("Fire1") && track) {
			mousePosition = Input.mousePosition;
			mousePosition = Camera.main.ScreenToWorldPoint (mousePosition);
			Vector2 t = Vector2.Lerp (transform.position, mousePosition, moveSpeed);
			transform.position = new Vector3 (t.x, t.y, transform.position.z);// Vector2.Lerp (transform.position, mousePosition, moveSpeed);
		}
		if (Input.GetButtonUp("Fire1") && Input.touches.Length == 0) {
			track = false;
		}
		if (Input.GetButtonDown("Fire1") || Input.touches.Length > 0){
			CastRay();
		}
	}
	void CastRay() {
		Vector3 mousePosition = Input.mousePosition;
		if (Input.touches.Length > 0 )
			mousePosition = Input.touches[0].position;

		Ray ray = CameraController.getMainCam().ScreenPointToRay(mousePosition);
		RaycastHit2D[] hit = Physics2D.RaycastAll (ray.origin, ray.direction, Mathf.Infinity);
		RaycastHit2D hit01 = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		foreach(RaycastHit2D aHit in hit){
			Debug.Log (aHit.collider);
		}
		if (hit01.collider !=null) {
			if (hit.Length > 1){
				if (hit[0].collider.GetComponent<Stamper>() != null && hit[1].collider.GetComponent<Stampable>() != null) {
					hit[0].collider.GetComponent<Stamper> ().Stamp(hit[1].collider.gameObject);
					hit[1].collider.GetComponent<Stampable> ().Stamp ();
				}
			}
			else if(hit01.transform==transform){
				if (hit01.collider.GetComponent<Stampable>() != null && !track) {
					Debug.Log ("tracking true");
					track = true;
				}
				else if(hit01.collider.GetComponent<Stampable> () != null && track){
					Debug.Log ("tracking false");
					track = false;
				}
			}
		}
	}

}
