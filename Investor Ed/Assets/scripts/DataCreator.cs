﻿using UnityEngine;
using System.Collections.Generic;

using TinyJson;


public class DataCreator : MonoBehaviour {
	private static string gameDataFileName = "Data.json";

	public static string LoadResourceTextfile(string path)
	{

		string filePath = path.Replace(".json", "");

		TextAsset targetFile = Resources.Load<TextAsset>(filePath);

		return targetFile.text;
	}

	public static Data getData(){
		string derr = LoadResourceTextfile(gameDataFileName);
		Data locale = derr.FromJson<Data>();
		//locale.construct();

		return locale;
	}

	void Start(){

		//LifeGenerator.createParty(getData());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
