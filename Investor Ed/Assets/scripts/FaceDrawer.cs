﻿using UnityEngine;
using System.Collections;

public class FaceDrawer : MonoBehaviour {

	public GameObject Face;
	public GameObject Nose;
	public GameObject Eyes;
	public GameObject Mouth;
	public GameObject Hair;
	public GameObject Backhair;
	public GameObject Extra1;
	public GameObject Extra2;
	public GameObject Suit;
	public GameObject Tie;
	public GameObject Shirt;


	private Object[] textures;

	//public void 
	public void randomFace(int seed){
		Random.seed = seed;

		textures = Resources.LoadAll("face/face", typeof(Texture2D));

		Texture2D tex = textures [Random.Range(0,textures.Length)] as Texture2D;

		Face.GetComponent<SpriteRenderer> ().sprite = Sprite.Create(tex,new Rect(0,0,tex.width,tex.height),new Vector2(.5f,.5f)) as Sprite;
		//go = GameObject.CreatePrimitive(PrimitiveType.Cube);
		textures = Resources.LoadAll("face/nose", typeof(Texture2D));

		Texture2D tex2 = textures [Random.Range(0,textures.Length)] as Texture2D;

		Nose.GetComponent<SpriteRenderer> ().sprite = Sprite.Create(tex2,new Rect(0,0,tex2.width,tex2.height),new Vector2(.5f,.5f)) as Sprite;

		if (Random.Range (0, 2) == 1) {
			//male
			textures = Resources.LoadAll ("face/mouth/male", typeof(Texture2D));

			tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

			Mouth.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;

			textures = Resources.LoadAll ("face/eyes/male", typeof(Texture2D));

			tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

			Eyes.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;

			if (Random.value > .25f) {
				textures = Resources.LoadAll ("face/hair", typeof(Texture2D));

				tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

				Hair.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;


				if (Random.value > .85f) {
					textures = Resources.LoadAll ("face/bottom_hair", typeof(Texture2D));

					tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

					Backhair.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;
				}
				else {
					Backhair.GetComponent<SpriteRenderer> ().sprite = null;
				}
			}
			else {
				Hair.GetComponent<SpriteRenderer> ().sprite = null;
				Backhair.GetComponent<SpriteRenderer> ().sprite = null;
			}

			if (Random.value > .65f) {
				textures = Resources.LoadAll ("face/facial_hair", typeof(Texture2D));

				tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

				Extra1.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;
			} else if (Random.value > .5f) {
				textures = Resources.LoadAll ("face/stuff", typeof(Texture2D));

				tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

				Extra1.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;
			} else {
				Extra1.GetComponent<SpriteRenderer> ().sprite = null;
			}

			if (Random.value > .8f) {
				textures = Resources.LoadAll ("face/stuff", typeof(Texture2D));

				tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

				Extra2.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;
			} else {
				Extra2.GetComponent<SpriteRenderer> ().sprite = null;
			}


		} else {
			//female

			textures = Resources.LoadAll ("face/mouth/female", typeof(Texture2D));

			tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

			Mouth.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;

			textures = Resources.LoadAll ("face/eyes/female", typeof(Texture2D));

			tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

			Eyes.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;

			if (Random.value > .05f) {
				textures = Resources.LoadAll ("face/hair", typeof(Texture2D));

				tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

				Hair.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;


				if (Random.value > .25f) {
					textures = Resources.LoadAll ("face/bottom_hair", typeof(Texture2D));

					tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

					Backhair.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;
				}
				else {
					Backhair.GetComponent<SpriteRenderer> ().sprite = null;
				}
			}
			else {
				Hair.GetComponent<SpriteRenderer> ().sprite = null;
				Backhair.GetComponent<SpriteRenderer> ().sprite = null;
			}

			if (Random.value > .5f) {
				textures = Resources.LoadAll ("face/stuff", typeof(Texture2D));

				tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

				Extra1.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;
			} else {
				Extra1.GetComponent<SpriteRenderer> ().sprite = null;
			}

			if (Random.value > .8f) {
				textures = Resources.LoadAll ("face/stuff", typeof(Texture2D));

				tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

				Extra2.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;
			} else {
				Extra2.GetComponent<SpriteRenderer> ().sprite = null;
			}

		}

		textures = Resources.LoadAll("broker/tie", typeof(Texture2D));

		tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

		Tie.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;

		textures = Resources.LoadAll("broker/shirt", typeof(Texture2D));

		tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

		Shirt.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;

		textures = Resources.LoadAll("broker/suit", typeof(Texture2D));

		tex2 = textures [Random.Range (0, textures.Length)] as Texture2D;

		Suit.GetComponent<SpriteRenderer> ().sprite = Sprite.Create (tex2, new Rect (0, 0, tex2.width, tex2.height), new Vector2 (.5f, .5f)) as Sprite;

	}

	// Use this for initialization
	void Start () {
		//this.randomFace (15);
	}
	private int i = 0;
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			this.randomFace (i);
			i++;
		}
	
	}
}
