﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stamper : MonoBehaviour {

	public float lockt = 0;
	public bool Accepted;
	public string NextScene = "NONE";
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (lockt > 0) {
			lockt -= Time.deltaTime;
		} else {
			lockt = 0;
		}
		
	}
	public void Stamp(GameObject approvalPaper){


		StartCoroutine(StampWait ());

		print("I stamped it");
		if (Accepted && lockt == 0) {
			lockt = 1;
			StartCoroutine(StampWaitAccept(approvalPaper));
		}
		if (!Accepted  && lockt == 0) {
			lockt = 1;
			StartCoroutine(StampWaitReject (approvalPaper));
		}
	}

	IEnumerator StampWait()
	{
		transform.position = new Vector3 (transform.position.x, transform.position.y - .05f, transform.position.z);
		yield return new WaitForSecondsRealtime(1);
		transform.position = new Vector3 (transform.position.x, transform.position.y + .05f, transform.position.z);
	}
	IEnumerator StampWaitAccept(GameObject approvalPaper)
	{
		if(NextScene.Equals("NONE")){
			SpriteRenderer[] approver = approvalPaper.GetComponentsInChildren<SpriteRenderer>();
			Color temp = approver [1].color;
			temp.a = 255f;
			approver [1].color = temp;
			yield return new WaitForSecondsRealtime (3);
			Winra.accept ();
			Color temp2 = approver [1].color;
			temp2.a = 0f;
			approver [1].color = temp2;
			lockt = 1;
		}
		else{
			Application.LoadLevel(NextScene);
		}
	}
	IEnumerator StampWaitReject(GameObject approvalPaper)
	{
		if(NextScene.Equals("NONE")){
			SpriteRenderer[] approver = approvalPaper.GetComponentsInChildren<SpriteRenderer>();
		
			Color temp = approver [2].color;
			temp.a = 255f;
			approver [2].color = temp;
			yield return new WaitForSecondsRealtime (3);
			Winra.reject ();
			Color temp2 = approver [2].color;
			temp2.a = 0f;
			approver [2].color = temp2;
			lockt = 1;
		}
		else{
			Application.LoadLevel(NextScene);
		}

	}
}
