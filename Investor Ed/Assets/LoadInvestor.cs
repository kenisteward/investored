﻿using UnityEngine;
using System.Collections;

public class LoadInvestor : MonoBehaviour {

	Investor broker;

	public GameObject nameText;
	public GameObject ageText;
	public GameObject sexText;

	public void Load(Investor b){
		broker = b;
		TextMesh name = nameText.GetComponent<TextMesh> ();
		TextMesh age = ageText.GetComponent<TextMesh> ();
		TextMesh sex = sexText.GetComponent<TextMesh> ();
		name.text = b.name;
		age.text = b.age + " ";
		sex.text = b.sex + " ";
	
	}
}
