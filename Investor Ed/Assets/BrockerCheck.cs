﻿using UnityEngine;
using System.Collections;
using System;
public class BrockerCheck : MonoBehaviour {

	public TextMesh problemCounter;

	Broker targetBroker;

	float countdown;

	void OnTriggerEnter2D(Collider2D coll) {
		Debug.Log ("COLLISION ");
		if (coll.gameObject.GetComponent<LoadBrokerSheet> () != null) {
			countdown = 5;
			targetBroker = coll.gameObject.GetComponent<LoadBrokerSheet> ().get();
			Debug.Log ("TARGET SET ");
		}

	}

	void OnTriggerExit2D(Collider2D coll) {
		Debug.Log ("COLLISION EXIT");
		if (coll.gameObject.GetComponent<LoadBrokerSheet> () != null) {
			targetBroker = null;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (countdown > 0) {
			try {
				if (targetBroker.complaints != null) {
					Debug.Log ("TARGET");
					string extraOs = "000";
					extraOs = extraOs.Substring ((targetBroker.complaints + " ").Length - 1);
					problemCounter.text = extraOs + targetBroker.complaints + "";
				}
			} catch (NullReferenceException e) {
				problemCounter.text = "000";
			}
			countdown -= Time.deltaTime;
		} else {
			problemCounter.text = "---";
		}
	}
}
