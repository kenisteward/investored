﻿using UnityEngine;
using System.Collections;
using System;
public class nasdaqTool : MonoBehaviour {

	public TextMesh textTool;

	Stock stock;

	float countdown;

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.GetComponent<LoadStock> () != null) {
			countdown = 10;
			stock = coll.gameObject.GetComponent<LoadStock> ().get();
			Debug.Log ("TARGET SET ");
		}

	}

	void OnTriggerExit2D(Collider2D coll) {
		Debug.Log ("COLLISION EXIT");
		if (coll.gameObject.GetComponent<LoadStock> () != null) {
			stock = null;
		}
	}

	// Update is called once per frame
	void Update () {
		if (countdown > 0) {
			try {
				if (stock.value != null) {
					Debug.Log ("TARGET");
					string extraOs = "";
					extraOs += "Value:\t\t" + stock.value +"\n"+
						"Volatility:"+stock.volatility+"\n\n" +
						"Type:" +
						"\n "+stock.type;
					textTool.text = extraOs;
				}
			} catch (NullReferenceException e) {
				textTool.text = "Stock N/A";
			}
			countdown -= Time.deltaTime;
		} else {
			textTool.text = "---";
		}
	}
}
