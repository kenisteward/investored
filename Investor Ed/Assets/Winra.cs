﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class Winra : MonoBehaviour {

	public List<TextMesh> moneybags;
	public List<TextMesh> repbags;

	public string basename = "cases/case_";

	public Transform clockArrow;

	public TextMesh ruleText;
	public TextMesh nRuleText;

	static public Winra WR;

	public Case current;


	public static float countdown;
	int maxtime = 300;





	private bool camReset;
	private float camCount;

	void Start(){
		WR = this;
		countdown = maxtime;
		if (PersistData.rules.Count == 0) {
			PersistData.rules.Add ("The broker must not have more than 35 complaints");
		}
		newDay ();
		newBroker ();

	}

	public static void accept(){
		CameraController.changeCam (1);
		PersistData.money += WR.current.result.pass.money;
		PersistData.reputation += WR.current.result.pass.reputation;
		WR.camCount = 3;
		WR.camReset = true;
	}

	public static void endDay(){
		CameraController.changeCam (4);
		WR.camCount = 5;
		WR.camReset = true;
		PersistData.broker = 0;
		WR.newDay ();
		WR.nRuleText.text = PersistData.rules [PersistData.rules.Count-1];
	}

	public static void reject(){
		CameraController.changeCam (2);
		PersistData.money += WR.current.result.fail.money;
		PersistData.reputation += WR.current.result.fail.reputation;
		WR.camCount = 3;
		WR.camReset = true;
	}

	public static void setCase(Case c){
		WR.current = c;
	}

	public void newBroker(){
		PersistData.broker++;
		//Kill all my children
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);
		}
		//Load a new broker
		if (PersistData.broker > 5) {
			endDay ();
		} else {
			try{
				this.GetComponent<loadJsonCase> ().loadCase (basename + PersistData.day + "_" + PersistData.broker);
			}catch(Exception e){
				//if out of money GAMEOVER
				if (PersistData.money <= 0) {
					Application.LoadLevel("Bankrupt");
				}
				else
				//if out of reputation GAMEOVER
				if (PersistData.reputation <= 0) {
					Application.LoadLevel("Nationalized");
				}
				else if (PersistData.day == 4) {
					Application.LoadLevel("End Scene");
				} else {
					endDay ();
				}
			}
		}
		//Transition to 

	}

	public void newDay(){
		countdown = maxtime;
		//add to day
		PersistData.day++;
		PersistData.broker = 0;
		//add to rules
		if (PersistData.day == 2) {
			PersistData.rules.Add ("Total commission fees must not exceed 5%");
		}
		if (PersistData.day == 3) {
			PersistData.rules.Add ("The stock should be different from the previous stock");
		}
		if (PersistData.day == 4) {
			PersistData.rules.Add ("Investors over 65 should not get volatile stocks");
		}
		if (PersistData.day == 5) {
			Application.LoadLevel("End Scene");
			//WIN
		}
		string consolidate = "";
		foreach (string rule in PersistData.rules) {
			consolidate += LoadSpeech.ResolveTextSize (rule, 30) +"\n";
		}
		ruleText.text = consolidate;
		//switch to rule screen
	}


	public void Update(){
		float oi = (countdown / maxtime);
		Debug.Log (oi);
		clockArrow.eulerAngles = (new Vector3(0,0,360 * oi));

		foreach (TextMesh tm in moneybags) {
			tm.text = PersistData.money +"$";
		}

		foreach (TextMesh tm in repbags) {
			tm.text = PersistData.reputation+" ";
		}

		if (camReset) {
			if (camCount <= 0) {
				//if out of money GAMEOVER
				if (PersistData.money <= 0) {
					Application.LoadLevel("Bankrupt");
				}
				//if out of reputation GAMEOVER
				if (PersistData.reputation <= 0) {
					Application.LoadLevel("Nationalized");
				}
				CameraController.changeCam (0);
				camReset = false;
				if (PersistData.broker == 0) {
					PersistData.money-=50;

					if (PersistData.day == 3) {
						Application.LoadLevel ("NASDAQTutorial");
					}
				}



				WR.newBroker ();

			} else {
				camCount -= Time.deltaTime;
			}
		} else {
			if (Input.GetKeyDown (KeyCode.A)) {
				accept ();
			}
			if (Input.GetKeyDown (KeyCode.D)) {
				reject ();
			}
		}

		//
		if (countdown <= 0) {
			//end day
			endDay();
		} else {
				countdown-= Time.deltaTime;
			//Debug.Log (countdown);
		}
			

	}
}
