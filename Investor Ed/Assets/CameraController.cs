﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	static CameraController CC;
	int selected;
	public Camera[] cameras;
	// Use this for initialization
	void Start () {
		CameraController.CC = this;
		turnOff ();
		//If any cameras were added to the controller, enable the first one
		if (cameras.Length>0)
		{
			selected = 0;
			cameras [0].gameObject.SetActive (true);
			Debug.Log ("Camera with name: " + cameras [0].ToString() + ", is now enabled");
		}
	}

	public static Camera getMainCam (){
		return CC.cameras [CC.selected];
	}

	public static void changeCam(int i){
		CC.turnOff ();
		CC.selected = i;
		CC.cameras [i].gameObject.SetActive (true);
	}
	public static Camera[] getCameras(){
		return CC.cameras;
	}

	public static CameraController get(){
		return CC;
	}

	public void turnOff(){
		//Turn all cameras off, except the first default one
		for (int i=1; i<cameras.Length; i++) 
		{
			cameras[i].gameObject.SetActive(false);
		}
	}
}
