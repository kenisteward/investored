﻿using UnityEngine;
using System.Collections.Generic;

public class Stock : MonoBehaviour {

	public string name;
	public int value;
	public int volatility;
	public List<int>history;
	public string type;

	public string ToString(){
		return "Stock: " + name + "\n"
			+ "value "+value + "\n"
			+ volatility + "\n"
			+ history.ToString() + "\n"
			+ type + "\n";
	}
}
