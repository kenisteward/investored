﻿using UnityEngine;
using System.Collections.Generic;
using System;
using TinyJson;

public class loadJsonCase : MonoBehaviour {
	//SDFSDFSDF
	public GameObject brokerPaper;
	public GameObject broker;
	public GameObject clientPaper;
	public GameObject NStock;
	public GameObject OStock;
	public GameObject Speech;
	public GameObject cash;
	public GameObject extraPaper;

	public GameObject Happy;
	public GameObject Sad;

	public string caseName = "case_Obs";
	// Use this for initialization
	void Start () {
		//this.loadCase (caseName);
	}

	public void loadCase(string path){
		//load JSON
		string filePath = path.Replace(".json", "");

		TextAsset targetFile = Resources.Load<TextAsset>(filePath);

		string src = targetFile.text;

		Case nCase = src.FromJson<Case>();


		GameObject gc = (GameObject)Instantiate (brokerPaper,transform);

		gc.GetComponent<LoadBrokerSheet> ().Load (nCase.broker);

		gc = (GameObject)Instantiate (broker,transform);

		gc.GetComponent<FaceDrawer> ().randomFace (nCase.broker.seed);

		gc = (GameObject)Instantiate (clientPaper,transform);

		gc.GetComponent<LoadInvestor> ().Load (nCase.investor);




		Debug.Log ("KEEP GOING");

		gc = (GameObject)Instantiate (Speech,transform);

		gc.GetComponent<LoadSpeech> ().Load (nCase.broker);



		TextMesh[] tmh = Happy.GetComponentsInChildren<TextMesh> ();

		tmh [0].text = nCase.result.pass.money + " ";
		tmh [1].text = nCase.result.pass.reputation + " ";
		tmh [2].text = LoadSpeech.ResolveTextSize(nCase.result.pass.text + " ",80);

		SpriteRenderer[] rmh = Happy.GetComponentsInChildren<SpriteRenderer> ();
		if (nCase.result.pass.happy) {
			rmh [0].enabled = false;
			rmh [1].enabled = true;
		} else {
			rmh [1].enabled = false;
			rmh [0].enabled = true;
		}

		tmh = Sad.GetComponentsInChildren<TextMesh> ();

		tmh [1].text = nCase.result.fail.money + " ";
		tmh [0].text = nCase.result.fail.reputation + " ";
		tmh [2].text = LoadSpeech.ResolveTextSize(nCase.result.fail.text + " ",80);

		rmh = Sad.GetComponentsInChildren<SpriteRenderer> ();
		if (nCase.result.fail.happy) {
			rmh [0].enabled = false;
			rmh [1].enabled = true;
		} else {
			rmh [1].enabled = false;
			rmh [0].enabled = true;
		}

		Debug.Log ("TRY");
		GameObject nc = null;
		try{
			nc = (GameObject)Instantiate (NStock,transform);

			nc.GetComponent<LoadStock> ().Load (nCase.new_stock);
		}catch(NullReferenceException e){
			Debug.Log ("FAIL");
			if( nc != null)
				Destroy (nc);
		}

		//Debug.Log (nCase.old_stock == null);
		Debug.Log ("TRY");
		GameObject oc = null;
		try{
			oc = (GameObject)Instantiate (OStock, transform);

			oc.GetComponent<LoadStock> ().Load (nCase.old_stock);
		}catch(NullReferenceException e){
			Debug.Log ("FAIL");
			if( oc != null)
				Destroy (oc);
		}

		GameObject dc = null;
		try{
			Debug.Log ("TRY AGAIN");
			for (int i = 0; i < nCase.money.value; i+=100) {
				dc = (GameObject)Instantiate (cash, transform);
				dc.GetComponent<Money> ().value = 100;
			}
		}catch(NullReferenceException e){
			Debug.Log ("OH NO");
			if( oc != null)
				Destroy (dc);
		}

		Winra.setCase (nCase);
	}

}
