﻿using UnityEngine;
using System.Collections;

public class Case : MonoBehaviour {

	public int case_id;
	public Broker broker;
	public Investor investor;
	public Stock old_stock;
	public Stock new_stock;
	public Result result;
	public Money money;

	public string ToString(){
		return "Case " + case_id + "\n"
		+ broker.ToString () + "\n"
			+investor.ToString()+"\n"
			+old_stock.ToString()+"\n"
			+new_stock.ToString()+"\n"
			+result.ToString()+"\n";
	}
}
