﻿using UnityEngine;

public class Result : MonoBehaviour {
	public Outro pass;
	public Outro fail;
	public string ToString(){
		return "Pass: " + pass.ToString () + "\n"
		+ "Reject: " + fail.ToString ();
	}
}
