﻿using UnityEngine;
using System.Collections.Generic;
public class Broker : MonoBehaviour {
	/**
	 * 
	 * 

	 */
	public string name;
	public int age;
	public int seed;
	public int complaints;
	public Dictionary<string,double> bills;
	public string talk;

	public string ToString(){
		return "Broker: " + name + "\n"
		+ "age "+age + "\n"
			+ seed + "\n"
			+ complaints + "\n"
			+ bills.ToString() + "\n"
			+talk + "\n";
	}
}
