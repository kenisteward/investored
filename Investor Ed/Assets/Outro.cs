﻿using UnityEngine;

public class Outro : MonoBehaviour {
	public bool happy;
	public int money;
	public int reputation;
	public string text;	
	public string ToString(){
		return "Resolution: " + text + "\n"
		+ money + "$\n"
		+ reputation + "\n"
		+ happy;
	}

}
