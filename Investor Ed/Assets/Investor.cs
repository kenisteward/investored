﻿using UnityEngine;
using System.Collections;

public class Investor : MonoBehaviour {

	public string name;
	public int age;
	public string sex;
	public string preference;

	public string ToString(){
		return "Investor: " + name + "\n"
			+ "age "+age + "\n"
			+ sex + "\n"
			+ preference + "\n";
	}
}
