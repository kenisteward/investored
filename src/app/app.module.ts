import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WINDOW, winFactory } from '../tokens';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertsComponent } from './alerts/alerts.component';
import { AlertService } from './alert.service';
import { ChatComponent } from './chat/chat.component';
import { WebsocketService } from './websocket.service';
import { InlineSVGModule } from 'ng-inline-svg';

@NgModule({
  declarations: [
    AppComponent,
    AlertsComponent,
    ChatComponent
  ],
  imports: [
    InlineSVGModule,
    NgbModule.forRoot(),
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: WINDOW,
      useFactory: winFactory,
      multi: false
    },
    AlertService,
    WebsocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
