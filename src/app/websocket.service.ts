import { Injectable, EventEmitter, Inject } from '@angular/core';
import { environment } from '../environments/environment';
import { WINDOW } from '../tokens';
import { Observable } from 'rxjs/Observable';
import { ChatMessage } from './chat/chat.component';

export interface AchievementMessage {
  type: 'achievement';
  title: 'Achievement Unlocked!';
  message: string;
}

export interface PresenceMessage {
  type: 'presence';
  message: string[];
}


declare type Messages = ChatMessage | AchievementMessage | PresenceMessage;

@Injectable()
export class WebsocketService {
  private _messages = new EventEmitter<Messages>();
  get messages() {
    return this._messages.asObservable();
  }
  private _ws;
  private _connected = false;
  private _connectComplete: EventEmitter<any> = new EventEmitter<any>();
  private _lastConnect;
  id: string;
  get connected(): boolean {
    return this._connected;
  }
  constructor( @Inject(WINDOW) private win: Window) { }

  connect(id: string) {
    if (this._connected === false) {
      this.id = id;
      if (environment.production) {
        this._ws = new WebSocket(`wss://${this.win.location.host}/connect?id=${id}`);
      } else {
        this._ws = new WebSocket(`ws://${this.win.location.host}/connect?id=${id}`);
      }

      this._ws.addEventListener('message', (event) => {
        this._messages.next(JSON.parse(event.data));
      });

      this._ws.addEventListener('open', (event) => {
        this._connected = true;
        this._lastConnect = event;
        this._connectComplete.next(event);
      });

      this._ws.addEventListener('close', (event) => {
        this._connected = false;
      });

      return this._connectComplete.asObservable();
    }

    return Observable.of(this._lastConnect);
  }

  send(message: any) {
    this._ws.send(JSON.stringify(message));
  }

  close() {
    this._ws.close();
  }

}
