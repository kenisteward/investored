import { Component, OnDestroy, ChangeDetectionStrategy, Input, ChangeDetectorRef, Inject, ElementRef, ViewChild } from '@angular/core';
import { WINDOW } from '../tokens';
import { environment } from '../environments/environment';
import { AlertService } from './alert.service';
import { WebsocketService, AchievementMessage } from './websocket.service';

@Component({
  selector: 'wi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  title = 'wi';
  connected = false;
  id: string;
  peopleOnline: string[];
  @ViewChild('userId') userId: ElementRef;
  @ViewChild('logButton') logButton: ElementRef;
  @ViewChild('frame') frame: ElementRef;
  @Input() messages: {type: string, message: string}[] = [];

  constructor(@Inject(WINDOW) private win: Window, private alerter: AlertService, public ws: WebsocketService) {
  }

  createConnection () {
    if ( this.userId.nativeElement.value) {
      this.id = this.userId.nativeElement.value;
      this.logButton.nativeElement.disabled = true;

      this.ws.connect(this.userId.nativeElement.value).subscribe((event) => {
          this.logButton.nativeElement.disabled = false;
          this.frame.nativeElement.src = '/assets/game/index.html?userId=' + this.id;
          this.ws.send({ type: 'presence', id: this.id});
          setInterval(() => {
            this.ws.send({ type: 'presence', id: this.id});
          }, 30000);
      });

      this.ws.messages.filter((m) => m.type === 'achievement').subscribe((m: AchievementMessage) => {
        this.alerter.alert({
          title: m.title,
          type: 'warning',
          message: m.message
        });
      });

      this.ws.messages.filter((m) => m.type === 'presence').subscribe((m: any) => {
        this.peopleOnline = m.message;
      });
    }
  }

  ngOnDestroy() {
    this.ws.close();
  }

}
