import { Component, OnInit } from '@angular/core';
import { IAlert, AlertService } from '../alert.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';

@Component({
  selector: 'wi-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {
  alerts: IAlert[] = [];
  constructor(private alerter: AlertService) { }

  ngOnInit() {
    this.alerter.alerts.subscribe((alert) => {
      this.alerts.push(alert);

      Observable.of(alert).delay(5000).subscribe((a) => {
        this.closeAlert(a);
      });
    });
  }

  closeAlert(a: IAlert) {
    const i = this.alerts.indexOf(a);
    if (i > -1) {
      this.alerts.splice(i, 1);
    }
  }

}
