import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked, Inject } from '@angular/core';
import { WebsocketService } from '../websocket.service';
import 'rxjs/add/operator/filter';
import { WINDOW } from '../../tokens';

export interface ChatMessage {
  type: 'chat';
  message: string;
  id: string;
}

@Component({
  selector: 'wi-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewChecked {

  private prevHeight;
  private minHeight;
  hide = false;
  messages: ChatMessage[] = [];
  @ViewChild('message') message: ElementRef;
  @ViewChild('wr') wrapper: ElementRef;
  @ViewChild('ct') chatTab: ElementRef;
  @ViewChild('ci') chatInputs: ElementRef;
  constructor(private ws: WebsocketService, @Inject(WINDOW) private win: Window) { }

  ngAfterViewChecked() {
    this.wrapper.nativeElement.scrollTop = this.wrapper.nativeElement.scrollHeight;
  }

  ngOnInit() {
    this.ws.messages.filter((m) => m.type === 'chat').subscribe((message: ChatMessage) => {
      this.messages = [...this.messages, message];
    });
  }

  sendMessage() {
    const m: ChatMessage = { type: 'chat', message: this.message.nativeElement.value, id: this.ws.id };
    this.ws.send(m);
    this.message.nativeElement.value = '';
  }

  toggleChat() {
    const currHeight = this.win.getComputedStyle(this.wrapper.nativeElement).maxHeight;
    if (currHeight === this.win.getComputedStyle(this.chatTab.nativeElement).height) {
      this.hide = false;
      this.wrapper.nativeElement.style.maxHeight = '100vh';
      this.wrapper.nativeElement.style.overflowY = 'scroll';
    } else {
      this.hide = true;
      this.wrapper.nativeElement.style.maxHeight = this.win.getComputedStyle(this.chatTab.nativeElement).height;
      this.wrapper.nativeElement.style.overflowY = 'hidden';
    }
  }

}
