import { Injectable, EventEmitter } from '@angular/core';

export interface IAlert {
  type: string;
  message: string;
  title: string;
}

@Injectable()
export class AlertService {
  private _alerts: EventEmitter<IAlert> = new EventEmitter<IAlert>();

  get alerts () {
    return this._alerts.asObservable();
  }

  constructor() { }

  alert(alert: IAlert) {
    this._alerts.next(alert);
  }

}
