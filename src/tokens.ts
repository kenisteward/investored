import { InjectionToken } from '@angular/core';

export const WINDOW = new InjectionToken<any>('Window object token');
export const winFactory = () => {
  return window;
};
