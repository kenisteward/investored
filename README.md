# Winra

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Raycasting Link

http://answers.unity3d.com/questions/968985/how-to-use-raycasting-2d-to-mouse-click-and-delete.html

## Building and publishing to azure

Make sure you have all the current node modules (and node) installed

Node.JS can be gotten from nodejs.org

After node is installed:

1. Switch to Winra Folder
2. Run ng build --prod
3. publish your changes to master ( this includes dist files)

Note: don't forget that if target ever changes these 2 lines need to be added
    ...
    <head>
    <base href="/assets/game/">
    ...

    and

    ...
        <canvas class="emscripten" id="canvas" oncontextmenu="event.preventDefault()" height="600px" width="960px" style="max-width:100%;max-height:100%;"></canvas>
   ...

    in the above width and height don't matter as much as max-width and max-height do.
## running the app locally

To run just the game you can simply run:

npm start

and then navigate to
localhost:1337/assets/game/index.html

To run the entire app run:

ng build
npm start

Note:  Before publishing always make sure to run:
ng build --prod

The environemnt in azure does not work correctly without it
